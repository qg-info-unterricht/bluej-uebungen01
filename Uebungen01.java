
/**
 * Beschreiben Sie hier die Klasse Uebungen01.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class Uebungen01
{
    // Instanzvariablen - ersetzen Sie das folgende Beispiel mit Ihren Variablen
    //private int x;

    /**
     * Konstruktor für Objekte der Klasse Uebungen01
     */
    public Uebungen01()
    {
        // Instanzvariable initialisieren
        //x = 0;
    }

    /**
     * Modulo: Berechne a mod b "von hand"
     * 
     * @param  a      Zahl die geteilt werden soll
     * @param  b      Zahl durch die geteilt wird.
     * @return        Rest der ganzzahligen Division 
     */
    public int MyModulo(int a, int b)
    {
        int remainder=0;
        // dein Code
        
        
        // Rückgabe
        return remainder;
    }
    
    /**
     * Switch: Vertausche a und b      
     * 
     * @param  a      Zahl 1
     * @param  b      Zahl 2
     */
    public void Switch(int a, int b) 
    {
          
    }
    
    /**
     * Pyramide: Volumen einer Pyramide      
     * 
     * @param
     * ....
     * @return
     */
    public void Pyramide() {
    }
    
    /**
     * Alterstest: Testet das Alter    
     * 
     * @param
     * ....
     * @return
     */
    public void Alterstest() {
    }
    
    
    // Erstelle weitere Methoden
}
